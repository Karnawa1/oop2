
package oop22;
import java.util.Arrays;
public class Oop22 {

    public static void main(String[] args) {
        Train[] trains = new Train[5];
        trains[0] = new Train("New York City", 2054, new Time(7, 30, 2));
        trains[1] = new Train("San Francisco", 9813, new Time(10, 15, 1));
        trains[2] = new Train("Houston", 4523, new Time(16, 0, 5));
        trains[3] = new Train("Houston", 5005, new Time(14, 30, 2));
        trains[4] = new Train("Chicago", 5087, new Time(14, 55, 43));
     

        var trainsCloned = trains.clone();
        Arrays.sort(trainsCloned, new TrainComparatorByTrainNumber());
        System.out.println(Arrays.toString(trainsCloned));


        Arrays.sort(trains, new TrainComparatorByDestination());
        System.out.println(Arrays.toString(trains));

    }
}
