

package oop25; 

import java.util.List;

public class Oop25 {
       public static void main(String[] args) {
Airline[] airlines = {
new Airline("New York", "NY123", "Boeing 747", "12:00", new String[]{"Monday", "Wednesday", "Friday"}),
new Airline("London", "LN456", "Airbus A380", "14:00", new String[]{"Tuesday", "Thursday", "Saturday"}),
new Airline("Paris", "PA789", "Boeing 787", "16:00", new String[]{"Monday", "Tuesday", "Wednesday"})
};
AirlineAggregator airlineAggregator = new AirlineAggregator(airlines);

List<Airline> flightsByDestination = airlineAggregator.getFlightsByDestination("New York");
System.out.println("Flights for destination New York:");
for (Airline flight : flightsByDestination) {
  System.out.println(flight);
}


List<Airline> flightsByDayOfWeek = airlineAggregator.getFlightsByDayOfWeek("Monday");
System.out.println("\nFlights for day of the week Monday:");
for (Airline flight : flightsByDayOfWeek) {
  System.out.println(flight);
}


List<Airline> flightsByDayOfWeekAndDepartureTime = airlineAggregator.getFlightsByDayOfWeekAndDepartureTime("Monday", "15:00");
System.out.println("\nFlights for day of the week Monday and departure time after 15:00:");
for (Airline flight : flightsByDayOfWeekAndDepartureTime) {
  System.out.println(flight);
}

    }
 }


