
package oop25;


import java.util.ArrayList;
import java.util.List;

public class AirlineAggregator {
  private Airline[] airlines;

  public AirlineAggregator(Airline[] airlines) {
    this.airlines = airlines;
  }

  public Airline[] getAirlines() {
    return airlines;
  }

  public void setAirlines(Airline[] airlines) {
    this.airlines = airlines;
  }

  public List<Airline> getFlightsByDestination(String destination) {
    List<Airline> flightsByDestination = new ArrayList<>();
    for (Airline airline : airlines) {
      if (airline.getDestination().equals(destination)) {
        flightsByDestination.add(airline);
      }
    }
    return flightsByDestination;
  }

  public List<Airline> getFlightsByDayOfWeek(String dayOfWeek) {
    List<Airline> flightsByDayOfWeek = new ArrayList<>();
    for (Airline airline : airlines) {
for (String dow : airline.getDaysOfWeek()) {
if (dow.equals(dayOfWeek)) {
flightsByDayOfWeek.add(airline);
break;
        }  
     }
  }
return flightsByDayOfWeek;
 }

public List<Airline> getFlightsByDayOfWeekAndDepartureTime(String dayOfWeek, String departureTime) {
List<Airline> flightsByDayOfWeekAndDepartureTime = new ArrayList<>();
for (Airline airline : airlines) {
for (String dow : airline.getDaysOfWeek()) {
if (dow.equals(dayOfWeek) && airline.getDepartureTime().compareTo(departureTime) > 0) {
flightsByDayOfWeekAndDepartureTime.add(airline);
break;
        }
     }
 }
return flightsByDayOfWeekAndDepartureTime;
  }
}
