

package oop21;


public class Oop21 {

    public static void main(String[] args) {
        Student[] students = new Student[10];
        students[0] = new Student("Smith", "J.D.", 123, new int[]{8, 9, 3, 9, 9});
        students[1] = new Student("Johnson", "A.B.", 124, new int[]{10, 10, 10, 10, 10});
        students[2] = new Student("Williams", "C.E.", 125, new int[]{9, 9, 10, 9, 9});
        students[3] = new Student("Jones", "D.F.", 126, new int[]{10, 10, 9, 9, 10});
        students[4] = new Student("Brown", "G.H.", 127, new int[]{9, 9, 9, 10, 9});
        students[5] = new Student("Davis", "I.J.", 128, new int[]{10, 9, 9, 9, 10});
        students[6] = new Student("Miller", "K.L.", 129, new int[]{9, 9, 9, 9, 9});
        students[7] = new Student("Wilson", "M.N.", 130, new int[]{10, 10, 10, 10, 10});
        students[8] = new Student("Moore", "O.P.", 131, new int[]{9, 10, 9, 9, 9});
        students[9] = new Student("Taylor", "Q.R.", 132, new int[]{10, 10, 10, 9, 10});

        System.out.println("Students with grades equal to 9 or 10:");
        for (Student student : students) {
            if (student.hasOutstandingGrades()) {
                student.printStudentInfo();
            }
        }

    }
}
