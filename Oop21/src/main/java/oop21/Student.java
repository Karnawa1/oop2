
package oop21;

public class Student {
    private final String surname;
    private final String initials;
    private final int groupNumber;
    private final int[] academicPerformance;

    
    public Student(String surname, String initials, int groupNumber, int[] academicPerformance) {
        this.surname = surname;
        this.initials = initials;
        this.groupNumber = groupNumber;
        this.academicPerformance = academicPerformance;
    }

   
    public boolean hasOutstandingGrades() {
        for (int grade : academicPerformance) {
            if (grade != 9 && grade != 10) {
                return false;
            }
        }
        return true;
    }

    public void printStudentInfo() {
        System.out.println("Surname: " + surname + ", Initials: " + initials + ", Group number: " + groupNumber);
    }
}
