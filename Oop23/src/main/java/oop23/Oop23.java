

package oop23;

public class Oop23 {

    public static void main(String[] args) {
        var customers = new Customers();
        customers.add(new Customer(1, "Smith", "John", "J", "123 Main St, Anytown USA", 1234567812, 123456));
        customers.add(new Customer(2, "Jones", "Jane", "D", "456 Park Ave, Anycity USA", 23456789, 98765));
        customers.add(new Customer(3, "Williams", "Bob", "P", "789 Elm St, Anyvillage USA", 345678934, 111111));
        customers.add(new Customer(4, "Jones", "Jane", "A", "321 Oak Rd, Anystate USA", 456789567, 222222));
        customers.add(new Customer(5, "Brown", "Emily", "G", "159 Pine St, Anycountry USA", 567896789, 333333));

        System.out.println(customers);
        System.out.println();

        var array = customers.getCustomersInAlphabetOrder();
        for (Customer element : array) {
            System.out.println(element);
        }

        System.out.println();

        var array2 = customers.getCustomersWithCreditCardNumberInInterval(345678934, 500000000);
        for (Customer element : array2) {
            System.out.println(element);
        }
    }

}
