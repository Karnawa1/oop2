
package oop23;


import java.util.ArrayList;
import java.util.Collections;

public class Customers {
    private final ArrayList<Customer> inner;

    public Customers() {
        inner = new ArrayList<>();
    }

    public void add(Customer customer) {
        inner.add(customer);
    }

    public ArrayList<Customer> getCustomersInAlphabetOrder() {
        var cloned = new ArrayList<>(inner);
        Collections.sort(cloned);

        return cloned;
    }

    public ArrayList<Customer> getCustomersWithCreditCardNumberInInterval(int from, int to) {
        var output = new ArrayList<Customer>();

        for (var element : inner) {
            if (element == null) {
                continue;
            }

            if (from <= element.getCreditCardNumber() && element.getCreditCardNumber() <= to) {
                output.add(element);
            }
        }

        return output;
    }

    @Override
    public String toString() {
        return inner.toString();
    }
}


