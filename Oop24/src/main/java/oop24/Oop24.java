
package oop24;

public class Oop24 {

    public static void main(String[] args) {
        var books = new Books();
        books.add(new Book(1, "Harry Potter and the Philosopher's Stone", new String[]{"J.K. Rowling"}, "Bloomsbury Publishing", 1997, 223, 10, "Hardcover"));
        books.add(new Book(2, "The Great Gatsby", new String[]{"F. Scott Fitzgerald"}, "J.B. Lippincott & Co.", 1925, 180, 15, "Paperback"));
        books.add(new Book(3, "To Kill a Mockingbird", new String[]{"F. Scott Fitzgerald"}, "Charles Scribner's Sons", 1960, 324, 12, "Hardcover"));
        books.add(new Book(4, "The Catcher in the Rye", new String[]{"J.D. Salinger"}, "Little, Brown and Company", 1951, 277, 9, "Paperback"));
        books.add(new Book(5, "The Lord of the Rings", new String[]{"J.R.R. Tolkien"}, "J.B. Lippincott & Co.", 1954, 1178, 20, "Hardcover"));

        System.out.println(books);
        System.out.println();

        System.out.println(books.getBooksByAuthor("F. Scott Fitzgerald"));
        System.out.println(books.getBooksByPublisher("J.B. Lippincott & Co."));
        System.out.println(books.getBooksAfterGivenYear(1951));

    }
}
